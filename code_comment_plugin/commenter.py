import idaapi
from ida_funcs import *
shouldGraph = False

x86registers = {
"eax" :"",
"ebx" :"",
"ecx" :"",
"edx" :"",
"esi" :"",
"edi" :"",
"ebp" :"",
"esp" :"",
"ax"  :"",
"ah"  :"",
"al"  :"",
"bx"  :"",
"bh"  :"",
"bl"  :"",
"cx"  :"",
"ch"  :"",
"cl"  :"",
"dx"  :"",
"dh"  :"",
"dl"  :"",
}

newFuncalls = {
    "??3@YAXPAX@Z" : "call delete(void*) operator", 
    "??2@YAPAXI@Z" : "call new(uint) operator"
}
opcodeSet = set([])
# Note all of these functions are not transferable.
localFuncComments = {
"sub_401078" : "",
"sub_401199" : "A copy buffer routine.",
"sub_4011DE" : "A copy buffer routine similar to sub_401199, but with different offsets.",
"sub_40123B" : "A copy buffer routine similar to sub_401199, but with different offsets.",
"sub_401280" : "A copy buffer routine similar to sub_401199, but with different offsets.",
"sub_4013CC" : "invoked in sub_401406. closes the winInet handles likely setup in sub_4010C0",
"sub_4012C5" : "Another function that sets up winInet functions and performs http post requests",
"sub_40101C" : "helper that invokes some copy buffer routines",
"sub_4010C0" : "Sets up a winInet and makes a Http request.",
"sub_401B7F" : "check exit code, if process specified running lookup namedPipe, read in a file setup invocation in cmd.exe.",
"sub_40138F" : "Reads an internet file and exits.",
"sub_401A0C" : "loop through buff. exit on delim 13 or 10, ret address at last index in buff if 13 ret address 2 bytes up.",
"sub_401A4F" : "Parser for control logic flags",
"sub_401C5D" : "sets up special stdio, env vars, & cmd.exe & gets its file attributes before invoking a child-process",
"sub_401ED9" : "",
"sub_4021F9" : "",
"sub_402637" : "",
"sub_40279A" : "",
"sub_4027D8" : "",
"sub_401946" : "",
"sub_4013A7" : "",
"sub_4025CE" : "",
"sub_401AEC" : "forks a child process that inits a console display mode and attaches to it",
"sub_402809" : "",
"sub_402A58" : "This function is a window listener",
"sub_402BB0" : "builds adobe reader path in app data",
"sub_402AB5" : "if sub_402BB0 failed, sub_402AB5 will try to buld adobe reader path differently.",
"sub_402C58" : "Invoked by thread. it probes user/comp name & version info to build a userAgent str then calls other funcs.",
"sub_402E87" : "This function looksup or creates a registry key",
"sub_402DAA" : "helper function for creating copying files even when dir needs to be created",
"sub_402ED8" : "This function reads an html site passes the buffer and size to sub_4032CB",
"sub_4032CB" : "invoked by sub_403085 sets up some buffers before calling sub_401406 where much of the control logic lays",
"sub_403085" : "This function parse the html for command instructions, like those on the html img tag",
"sub_403012" : "parses the html src tag returns a boolean value depending on content",
"sub_4033F4" : "Invoked by a thread create, downloads a file from a url to a temp dir and executes it as a shell script",
"sub_403339" : "",
"sub_403798" : "builds up the alphabet for a cesar cipher.",
"sub_4035C2" : "creates a file, executes it as a shell script.",
"sub_401406" : "Creates pipes and threads & will determine what type of action to take to fetch or push files to the infect machine."
}

libFuncComments = {
"ds:Sleep"                     : "Suspends the execution of the current thread until the time-out interval elapses.",
"ds:GetMessageA"               : "gets msg from calling thread's queue & dispatches incoming sent msgs until a posted msg is avail.",
"ds:DeleteFileA"               : "Deletes an existing file.",
"memset"                       : "Sets  1st num bytes of the block of mem ptr to the specified value (interpreted as a ubyte).",
"memcpy"                       : "Copies the val of num bytes from  src location directly to the mem block pointed to by dest.",
"ds:RegisterClassExA"          : "Registers a window class for subsequent use in calls to the CreateWindow[Ex] function.",
"ds:CreateWindowExA"           : "Creates an overlapped, pop-up, or child window with an extended window style.",
"ds:CreateThread"              : "Creates a thread to execute within the virtual address space of the calling process.",
"ds:CloseHandle"               : "Closes an open object handle.",
"ds:TranslateMessage"          : "Translates virtual-key messages into character messages. ",
"ds:DispatchMessageA"          : "Dispatches a msg to a window proc. typically used to dispatch a msg retrieved by GetMessage.",
"ds:InternetOpenA"             : "Initializes an application's use of the WinINet functions.",
"ds:InternetConnectA"          : "Opens an File Transfer Protocol (FTP) or HTTP session for a given site.",
"ds:HttpOpenRequestA"          : "Creates an HTTP request handle.",
"strcpy"                       : " cpys the C str ptr src into the arr ptr dest, including the \0 char (and stopping at that point).",
"strlen"                       : "The length of a C string upto \0.",
"strcat"                       : "Appends a copy of the source string to the destination string.",
"ds:lstrcatA"                  : "Appends one string to another.",
"ds:HttpSendRequestA"          : "Sends the specified request to the HTTP server.",
"ds:GetLastError"              : "Retrieves the calling thread's lasterror code. Lasterror code is maintained on a per-thread basis.",
"ds:InternetSetOptionA"        : "Sets an Internet option.",
"ds:InternetReadFile"          : "Reads data from a handle opened by the InternetOpenUrl, FtpOpenFile, or HttpOpenRequest function.",
"ds:GetComputerNameA"          : "Retrieves the NetBIOS name of the local computer. This name is established at system startup.",
"ds:CreatePipe"                : "Creates an anonymous pipe, and returns handles to the read and write ends of the pipe.",
"ds:sprintf"                   : "Composes a string with the same text that would be printed if format was used on printf,",
"ds:_strcmpi"                  : "Performs a case-insensitive comparison of strings.",
"GetUserNameExA"               : "Retrieves the name of the user or other security principal associated with the calling thread.",
"ds:atoi"                      : "Parses the C-str interpreting its content as a number, which is returned as a value of type int.",
"ds:atol"                      : "Parses the C-str interpreting its content as a number, returns as a value of type long int.",
"ds:TerminateProcess"          : "Terminates the specified process and all of its threads.",
"ds:CreateFileA"               : "Creates or opens a file or I/O device. ",
"ds:WriteConsoleInputA"        : "Writes data directly to the console input buffer.",
"ds:CreateProcessA"            : "Creates a new proc and its thread. The new proc is in the security context of the calling proc.",
"ds:AttachConsole"             : "Attaches the calling process to the console of the specified process.",
"ds:GetExitCodeProcess"        : "Retrieves the termination status of the specified process.",
"ds:PeekNamedPipe"             : "Copies data from a named or anonymous pipe into a buffer without removing it from the pipe",
"ds:ReadFile"                  : "Reads data from the specified file or input/output (I/O) device.",
"ds:GetFileAttributesA"        : "Retrieves file system attributes for a specified file or directory.",
"ds:OpenProcess"               : "Opens an existing local process object.",
"ds:OpenProcessToken"          : "The OpenProcessToken function opens the access token associated with a process.",
"Process32First"               : "Retrieves information about the first process encountered in a system snapshot.",
"Process32Next"                : "Retrieves information about the next process recorded in a system snapshot.",
"ds:OpenSCManagerA"            : "Est a connection to SCmanager on the specified comp & opens the specified SCmanager database.",
"ds:CloseServiceHandle"        : "Closes a handle to a service control manager or service object.",
"ds:GetLogicalDrives"          : "Retrieves a bitmask representing the currently available disk drives.",
"CreateToolhelp32Snapshot"     : "Takes a snapshot of the specified procs, as well as heaps, modules, & threads used by these procs.",
"ds:CreateProcessAsUserA"      : "Creates a new process and its primary thread. Runs in the sec. context of user reped by spec token.",
"ds:GetSystemDirectoryA"       : "Retrieves the path of the system directory. ",
"ds:ExpandEnvironmentStringsA" : "Expands env-variable strings and replaces them with the values defined for the current user.",
"ds:GetConsoleDisplayMode"     : "Retrieves the display mode of the current console.",
"ds:WaitForSingleObject"       : "Waits until the specified object is in the signaled state or the time-out interval elapses.",
"ds:SetCurrentDirectoryA"      : "Changes the current directory for the current process.",
"ds:GetWindowsDirectoryA"      : "Retrieves the path of the Windows directory.",
"ds:InternetQueryOptionA"      : "Queries an Internet option on the specified handle.",
"ds:HttpAddRequestHeadersA"    : "Adds one or more HTTP request headers to the HTTP request handle.",
"_EH_prolog"                   : "Exception Handler proglog",
"ds:GetDriveTypeA"             : "Determines whether a disk drive is a removable, fixed, CD-ROM, RAM disk, or network drive.",
"ds:GetVolumeInformationA"     : "Retrieves information about the file system and volume associated with the specified root dir.",
"ds:OpenServiceA"              : "Opens an existing service.",
"ds:ControlService"            : "Sends a control code to a service.",
"ds:strrchr"                   : "Returns a pointer to the last occurrence of character in the C string str.",
"ds:GetFileSize"               : "Retrieves the size of the specified file, in bytes.",
"ds:sscanf"                    : "Read formatted data from string",
"ds:WriteFile"                 : "Writes data to the specified file or input/output (I/O) device.",
"ds:StartServiceA"             : "Starts a service.",
"URLDownloadToFileA"           : "Downloads bits from the Internet and saves them to a file.",
"ds:GetCurrentProcess"         : "Retrieves a pseudo handle for the current process.",
"ds:DefWindowProcA"            : "Calls default win procedure to provide default processing for win msgs that an app doesn't process.",
"ds:GetModuleFileNameA"        : "Retrieves the fully qualified path for the file that contains the specified module.",
"ds:GetUserProfileDirectoryA"  : "Retrieves the path to the root directory of the specified user's profile.",
"ds:ShellExecuteA"             : "Performs an operation on a specified file.",
"ds:CreateMutexA"              : "Creates or opens a named or unnamed mutex object.",
"ds:GetVersionExA"             : "return the version that the application is manifested for in future releases. ",
"ds:GetUserNameA"              : "Retrieves the name of the user associated with the current thread.",
"ds:GetLocalTime"              : "Retrieves the current local date and time.",
"ds:SHCreateDirectoryExA"      : "Creates a new file system folder, with optional security attributes.",
"ds:CopyFileA"                 : "Copies an existing file to a new file.",
"ds:GetSystemTime"             : "Retrieves the current system date and time. ",
"ds:SystemTimeToFileTime"      : "Converts a system time to file time format. System time is based on (UTC).",
"ds:SetFileTime"               : "Sets the date & time that the specified file or dir was created, last accessed, or last modified.",
"ds:RegCreateKeyExA"           : "Creates speced reg key. If the key exists, func opens it. Note key names are not case sensitive.",
"ds:lstrlenA"                  : "Determines the length of the specified string (not including the terminating null character).",
"ds:RegSetValueExA"            : "Sets the data and type of a specified value under a registry key.",
"ds:RegCloseKey"               : "Closes a handle to the specified registry key.",
"ds:InternetOpenUrlA"          : "Opens a resource specified by a complete FTP or HTTP URL.",
"ds:InternetCloseHandle"       : "Closes a single Internet handle.",
"ds:strstr"                    : " rets a ptr to the first occurrence of str2 in str1, or a nullptr if str2 is not part of str1.",
"ds:_strnicmp"                 : "cmps, w\out case sensitivity, the string ptr s1 to the string ptr s2, for at most len chars.",
"ds:strchr"                    : "Locate first occurrence of character in string",
"__alloca_probe"               : "ensures that alloca() call returns a pointer aligned to 16 bytes boundary.",
"ds:GetTempFileNameA"          : "Creates a name for a temporary file. ",
"ds:RegDeleteValueA"           : "Removes a named value from the specified registry key.",
"ds:ExitProcess"               : "Ends the calling process and all its threads.",
"ds:__set_app_type"            : "Sets the current application type.",
"ds:__p__fmode"                : "internal func. Points to _fmode global var, which specs default file translation mode for I/O ops.",
"ds:__p__commode"              : "internal func. Points to _commode global var, which specs default file commit mode for I/O ops.",
"nullsub_1"                    : "Place holder for funcs that do nothing but return potentially b\c of ifdef or base virtual func",
"ds:__setusermatherr"          : "Specifies a user-supplied rountine to handle math errors, instead of the _matherr routine.",
"__setdefaultprecision"        : "sets the floating point precision.",
"_initterm"                    : "Internal methods that walk a table of function pointers and initialize them.",
"ds:__getmainargs"             : "Invokes command-line parsing and copies the arguments to main() back through the passed pointers.",
"ds:GetStartupInfoA"           : "Retrieves the contents of  STARTUPINFO struct that specifies when calling process was created.",
"ds:GetModuleHandleA"          : "Retrieves a handle for the specified module. Module must have been loaded by calling process.",
"_WinMain@16"                  : "main function, entry point for exe",
"ds:exit"                      : "Terminates the calling process. The exit function terminates it after cleanup.",
"ds:_exit"                     : "Terminates the calling process. _exit terminate it immediately.",
"_XcptFilter"                  : "Identifies the exception and the related action to be taken.",
"_controlfp"                   : "Gets and sets the floating-point control word. ",
"ds:SetStdHandle"              : "Sets the handle for the specified std device (stdio, or standard error)."
}

class myplugin_t(idaapi.plugin_t):
    flags = idaapi.PLUGIN_UNL
    comment = "This is a comment"
    help = "This is help"
    wanted_name = "My Python plugin"
    wanted_hotkey = "Alt-F8"

    def init(self):
        return idaapi.PLUGIN_OK

    def run(self, arg):
        print "Start"
        genAllFuncComments()
        #print opcodeSet
        #getBasicBlock(getWinMainFunction())
        #commentfunction(getWinMainFunction())

    def term(self):
        pass

def PLUGIN_ENTRY():
    return myplugin_t()

def getFuncByName(searchFuncName):
    for i in range(0,get_func_qty()):
        func = getn_func(i)
        funcName = get_func_name2(func.startEA)
        if funcName == searchFuncName:
            return func
        #else:
        #    print"function[{}] = {}".format(i,funcName)
    return None

def getWinMainFunction():
    return getFuncByName("_WinMain@16");

def genAllFuncComments():
    for i in range(0,get_func_qty()):
        func = getn_func(i)
        commentfunction(func)

def commentfunction(func):
    flow = idaapi.FlowChart(func)
    funcEndEA = idc.PrevHead(func.endEA)
    clearEA(func.startEA,funcEndEA)
    for block in flow:
        clearEA(block.startEA, idc.PrevHead(block.endEA))
        getInstructions(block)
    
    prevCom = GetCommentEx(func.startEA, 0)
    if(prevCom == None):
        prevCom = ""
    else:
        prevCom = "{}\n".format(prevCom)

    funcName = get_func_name2(func.startEA)
    MakeComm(func.startEA, "{}start function {}".format(prevCom, funcName))

    for block in flow:
        commentBlockEA(block)

    prevEndCom = GetCommentEx(funcEndEA, 0)
    if(prevEndCom == None):
        prevEndCom = ""
    else:
        prevEndCom = "{}\n".format(prevEndCom)
    MakeComm(funcEndEA, "{}end function {}".format(prevEndCom, funcName))

def clearEA(startEA,endEA):
    MakeComm(startEA,"");
    MakeComm(endEA,"");

def commentBlockEA(block):
    blockEndEA = idc.PrevHead(block.endEA)
    prevCom = GetCommentEx(block.startEA, 0)
    if(prevCom == None):
        prevCom = ""
    else:
        prevCom = "{}\n".format(prevCom)

    MakeComm(block.startEA, "{}start block {}"
         .format(prevCom,block.id))

    prevEndCom = GetCommentEx(blockEndEA, 0)
    if(prevEndCom == None):
        prevEndCom = ""
    else:
        prevEndCom = "{}\n".format(prevEndCom)

    succBlockIds = []
    for succBlock in block.succs():
        succBlockIds.append(succBlock.id)
    MakeComm(blockEndEA, "{}end block {} successors are block ids: {}"
         .format(prevEndCom, block.id, succBlockIds))

def commentCallInstruction(opcode, ea):
    if(opcode == "call"):
        operand = idc.GetOpnd(ea, 0)
        desc = libFuncComments.get(operand)
        localFndesc = localFuncComments.get(operand)
        newFndesc = newFuncalls.get(operand)
        if(desc != None):
            MakeComm(ea,desc)
        elif(localFndesc != None):
            MakeComm(ea,localFndesc)
        elif(newFndesc != None):
            MakeComm(ea, newFndesc)
        fptr = x86registers.get(operand)
        if(fptr):
            desc = libFuncComments.get(fptr)
            if(not desc):
                desc = localFuncComments.get(operand);
            if(desc):
                MakeComm(ea,"call fptr {} that {}".format(fptr, desc))
            x86registers["eax"] = "return value of {}".format(fptr)
        else:
            x86registers["eax"] = "return value of {}".format(operand)

        #if(not (operand in newFuncalls.keys()) and
        #   not (operand in x86registers.keys()) and
        #   None == desc and
        #   None == localFuncComments.get(operand)):
        #    print "call {}".format(operand)

def commentPushInstruction(opcode, ea):
    if(opcode == "push"):
        operand = idc.GetOpnd(ea, 0)
        if(operand == "ebp"):
            MakeComm(ea,"save the old base pointer value")
        elif(operand[0] == '[' and operand[-1] == ']'):
            MakeComm(ea,"push the 4 bytes at address {} onto the stack.".format(operand[1:-1]))
        else:
            isOperand1AReg = (operand in x86registers.keys())
            if(isOperand1AReg and x86registers[operand] !=""):
                MakeComm(ea,"push {} which is {} onto the stack".format(operand, x86registers[operand]))
            else:
                MakeComm(ea,"push {} onto the stack".format(operand))

def commentXORInstruction(opcode, ea):
    if(opcode == "xor"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        if(operand1 == operand2):
            MakeComm(ea, "set {} to 0".format(operand1));
            x86registers[operand1] = "0"
        else:
            MakeComm(ea, "store in {} the set bits that don't overlap between {} and {}".format(operand1,operand2,operand1))


def commentIncInstruction(opcode, ea):
    if(opcode == "inc"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "increment {} by 1".format(operand1));

def commentDecInstruction(opcode, ea):
    if(opcode == "dec"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "decrement {} by 1".format(operand1));

def commentAndInstruction(opcode, ea):
    if(opcode == "and"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        if(operand2 == '0'):
            x86registers[operand1] = "0"
            MakeComm(ea, "set {} to 0".format(operand1));
        isOperand2AReg = (operand1 in x86registers.keys())
        if(isOperand2AReg):
            MakeComm(ea, "And {} with {}".format(operand1,operand2));
            x86registers[operand1] = ""
        else:
            i = 0
            if(operand2[-1] == 'h'):
                i = int(operand2[:-1], 16)
            else:
                i = int(operand2)
            MakeComm(ea, "clear all the bits in {} not set by".format(operand1,i));
            x86registers[operand1] = ""


def commentSubInstruction(opcode, ea):
    if(opcode == "sub"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        if(operand1 == "esp"):
            i = int(operand2[:-1], 16)
            MakeComm(ea, "using the stack pointer make room for {} bytes on the stack.".format(i))
        else:
            MakeComm(ea, "subtract {} from {}".format(operand2,operand1))


def commentIMulInstruction(opcode, ea):
    if(opcode == "imul"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        operand3 = idc.GetOpnd(ea, 2)
        isOperand1AReg = (operand1 in x86registers.keys())
        isOperand2AReg = (operand2 in x86registers.keys())
        x86registers[operand1] =""
        if(operand3 == None):
            if(isOperand1AReg and isOperand2AReg):
                MakeComm(ea,"Multiply {} by {} store in {}".format(operand1,operand2,operand1))
            elif(operand2[0] == '[' and operand2[-1] == ']'):
                MakeComm(ea,"multiply the contents of {} by the contents of the memory location {}. Store the result in {}."
                         .format(operand1,operand2[1:-1],operand1))
        else:
            i = 0
            if(operand3[-1] == 'h'):
                i = int(operand3[:-1], 16)
            else:
                i = int(operand3)
            if(isOperand1AReg and isOperand2AReg):
                MakeComm(ea,"Multiply {} by {} store in {}".format(operand2,i,operand1))
            elif(operand2 ==""):
                MakeComm(ea,"Multiply {} by constant {} store in {}".format(operand1,i,operand1))
            elif(operand2[0] == '[' and operand2[-1] == ']'):
                MakeComm(ea,"multiply the const number {} by the contents of the memory location {}. Store the result in {}."
                         .format(i,operand2[1:-1],operand1))

def commentIDivInstruction(opcode, ea):
    if(opcode == "idiv"):
        operand1 = idc.GetOpnd(ea, 0)
        if operand1 == "":
            operand1 = idc.GetOpnd(ea, 1)
        isOperand1AReg = (operand1 in x86registers.keys())
        if(not isOperand1AReg and operand1[0] == '[' and operand1[-1] == ']'):
            MakeComm(ea,"divide the contents of EDX:EAX by the value stored at memory location {}. Place the quotient in EAX and the remainder in EDX.".format(operand1[1:-1]))
        else:
            MakeComm(ea,"divide the contents of EDX:EAX by the contents of {}. Place the quotient in EAX and the remainder in EDX.".format(operand1))
        x86registers["eax"] =""
        x86registers["edx"] =""



def commentAddInstruction(opcode, ea):
    if(opcode == "add"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        if(operand1 == "esp"):
            i = int(operand2[:-1], 16)
            MakeComm(ea, "move the stack pointer up {} bytes.".format(i))
        else:
            MakeComm(ea, "add {} to {}".format(operand2,operand1))


def commentTestInstruction(opcode, ea):
    if(opcode == "test"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        if(operand1 == operand2):
             MakeComm(ea, "check if {} is 0 by anding with self. Set ZF flag if so.".format(operand1))
        else:
            MakeComm(ea, "test equality by anding {} with {}. Set ZF flag accordingly.".format(operand1,operand2))

def commentJZInstruction(opcode, ea):
    if(opcode == "jz"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if zero to label {} if ZF flag set, else go to next instruction".format(operand1));

def commentJNZInstruction(opcode, ea):
    if(opcode == "jnz"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if not zero to label {} if ZF flag is not set, else go to next instruction".format(operand1));

def commentJmpInstruction(opcode, ea):
    if(opcode == "jmp"):
        operand1 = idc.GetOpnd(ea, 0)
        jumpType = "label"
        if(operand1.startswith("ds:")):
            jumpType ="function"
        MakeComm(ea, "unconditional jump to {} {}".format(jumpType, operand1));

def commentJleInstruction(opcode, ea):
    if(opcode == "jle"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if less-than or equal to label {} if ZF flag set OR if sign flag (SF)  does not equal overflow flag (OF), else go to next instruction".format(operand1));

def commentJlInstruction(opcode, ea):
    if(opcode == "jl"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if less-than to label {} if sign flag (SF)  does not equal overflow flag (OF), else go to next instruction".format(operand1));

def commentJgInstruction(opcode, ea):
    if(opcode == "jg"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if greater-than to label {} if SF = OF and ZF = 1, else go to next instruction".format(operand1));

def commentJgeInstruction(opcode, ea):
    if(opcode == "jge"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if greater-than or equal to label {} if SF = OF and ZF = 0, else go to next instruction".format(operand1));

def commentJaInstruction(opcode, ea):
    if(opcode == "ja"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if above to label {} if CF = 0 and ZF = 0, else go to next instruction".format(operand1));

def commentJbInstruction(opcode, ea):
    if(opcode == "jb"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if below to label {} if CF = 1, else go to next instruction".format(operand1));

def commentJnbInstruction(opcode, ea):
    if(opcode == "jnb"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if not below to label {} if CF = 0, else go to next instruction".format(operand1));

def commentJbeInstruction(opcode, ea):
    if(opcode == "jbe"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "jump if below or equal to label {} if CF = 1 or ZF = 1, else go to next instruction".format(operand1));



def commentLoopInstructions(opcode, ea):
    if(opcode != "" and opcode[0] == 'j'):
        operand = idc.GetOpnd(ea, 0)
        if (operand.startswith("loc_")):
            jmpLoc = int(operand[4:], 16)
            if(jmpLoc < ea):
                prevEndCom = GetCommentEx(ea, 0)
                if(prevEndCom == None):
                    prevEndCom = ""
                else:
                    prevEndCom = "{}\n".format(prevEndCom)
                MakeComm(ea, "{}jump to start of loop at {}".format(prevEndCom, operand));

def commentPopInstruction(opcode, ea):
    if(opcode == "pop"):
        operand1 = idc.GetOpnd(ea, 0)
        isOperand1AReg = (operand1 in x86registers.keys())
        if(isOperand1AReg):
            x86registers[operand1] = ""
            MakeComm(ea, "pop the top element of the stack into {}.".format(operand1))
        elif(operand1[0] == '[' and operand1[-1] == ']'):
            MakeComm(ea, "pop the top element of the stack into memory bytes starting at location {}".format(operand1[1:-1]))

def commentMovInstruction(opcode, ea):
    if(opcode == "mov"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        isOperand1AReg = (operand1 in x86registers.keys())
        if(operand1 == "ebp" and operand2 == "esp"):
            MakeComm(ea, "set the new base pointer value to the stack pointer.")
        elif(isOperand1AReg and (libFuncComments.get(operand2)  != None or
           localFuncComments.get(operand2) != None)):
           x86registers[operand1] = operand2
           MakeComm(ea, "Move function ptr {} into {}".format(operand2, operand1))
        elif(operand1[0] == '[' and operand1[-1] == ']'):
            MakeComm(ea, "Move the contents of {} into the 4 bytes at memory address {}.".format(operand2,operand1))
        elif(isOperand1AReg):
            if(operand2[0] == '[' and operand2[-1] == ']'):
                MakeComm(ea, "Move the bytes in memory at the address contained in {} in {}".format(operand2[1:-1],operand1))
                x86registers[operand1] = ""
            else:
                isOperand2AReg = (operand2 in x86registers.keys())
                if(isOperand2AReg):
                    x86registers[operand1] = x86registers[operand2]
                    MakeComm(ea, "Move  {} into {}".format(operand2,operand1))
                else:
                    iStr = ""
                    if(operand2[-1] == 'h'):
                        iStr = str(int(operand2[:-1], 16))
                    else:
                        iStr = operand2
                    MakeComm(ea, "Move  {} into {}".format(iStr,operand1))
                    x86registers[operand1] = iStr
        elif(operand2 == "eax"):
            searchStr = "return value of "
            if(x86registers["eax"].startswith(searchStr)):
                index = x86registers["eax"].index(searchStr)+len(searchStr)
                MakeComm(ea, "Move the return value  in eax from function {} into {}".format(x86registers["eax"][index:],operand1))

def commentCdqInstruction(opcode, ea):
    if(opcode == "cdq"):
        MakeComm(ea, "instruction extends the sign bit of EAX into the EDX register.")
    x86registers["edx"] = "4294967295" #0xFFFFFFFF

def commentSetnlInstruction(opcode, ea):
    if(opcode == "setnl"):
        operand1 = idc.GetOpnd(ea, 0)
        MakeComm(ea, "Sets the byte in {} to 1 if the Sign Flag equals the Overflow Flag, otherwise sets  {} to 0."
                 .format(operand1,operand1))
    #Note: byte registers should make changes to Dword registers, might be able to hold off on this
    # see 0x402DF5 for example

def commentStosdInstruction(opcode, ea):
    if(opcode == "stosd"):
        MakeComm(ea, "Stores doubleword from EAX b\c of `d` at end of instruction opcode, respectively,\ninto the destination operand. The destination operand is a memory location, the address of which is read from EDI.")
    #Note example at 0x401A84

def commentSarInstruction(opcode, ea):
    if(opcode == "sar"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        if( operand1 in x86registers.keys()):
            x86registers[operand1] =""
        MakeComm(ea,"shift {} right by {} but preserve the most-significant bit (MSB)".format(operand1,operand2))

def commentShlInstruction(opcode, ea):
    if(opcode == "shl"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        if( operand1 in x86registers.keys()):
            x86registers[operand1] =""
        MakeComm(ea,"shift {} left by {}".format(operand1,operand2))

def commentShrInstruction(opcode, ea):
    if(opcode == "shr"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        if( operand1 in x86registers.keys()):
            x86registers[operand1] =""
        MakeComm(ea,"shift {} right by {}".format(operand1,operand2))

def commentLeaInstruction(opcode, ea):
    if(opcode == "lea"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        MakeComm(ea, "the value in address {} is placed in {}".format(operand2,operand1))

def commentMovsxInstruction(opcode, ea):
    if(opcode == "movsx"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        MakeComm(ea, "Copies the contents of {} to {} and sign extends the value.".format(operand2,operand1))
        x86registers[operand1] =""

def commentMovzxInstruction(opcode, ea):
    if(opcode == "movzx"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        MakeComm(ea, "Copies the contents of {} to {} and zero extends the value.".format(operand2,operand1))
        x86registers[operand1] =""

def commentLeaveInstruction(opcode, ea):
    if(opcode == "leave"):
        MakeComm(ea, "Equivalent to mov esp, ebp AND pop ebp. in that leave deallocate local variables by restoring the stack pointer to the base pointer address and then resets to the caller's bp  by poping.");

def commentOrInstruction(opcode, ea):
    if(opcode == "or"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        MakeComm(ea, "Bitwise inclusive OR operation between the destination {} and source {} operands and stores the result in the destination {}".format(operand1,operand2, operand1))
        x86registers[operand1] =""

def commentCmpInstruction(opcode, ea):
    if(opcode == "cmp"):
        operand1 = idc.GetOpnd(ea, 0)
        operand2 = idc.GetOpnd(ea, 1)
        isOperand1AReg = (operand1 in x86registers.keys())
        isOperand2AReg = (operand2 in x86registers.keys())
        isOperand1AMem = False
        isOperand2AMem = False
        isOperand2AConst = False
        if not isOperand1AReg:
            isOperand1AMem = (operand1[0] == '[' and operand1[-1] == ']')
            if not isOperand1AMem:
                isOperand1AMem = (operand1.startswith("offset") or
                    operand1.startswith("(offset") or operand1.startswith("byte_"))
        if not isOperand2AReg:
            isOperand2AMem = (operand2[0] == '[' and operand2[-1] == ']')
            if not isOperand2AMem:
                isOperand2AMem = (operand2.startswith("offset") or
                    operand2.startswith("(offset") or operand2.startswith("byte_"))
        if not (isOperand2AMem or isOperand2AReg):
            isOperand2AConst = True
        i = 0
        if isOperand2AConst:
            if(operand2[-1] == 'h'):
                i = int(operand2[:-1], 16)
            else:
                i = int(operand2)

        if isOperand1AReg and isOperand2AReg:
            MakeComm(ea, "If register {} is equal to register {} set ZF"
                     .format(operand1,operand2))
        elif isOperand1AReg and isOperand2AMem:
            MakeComm(ea, "If register {} is equal to the bytes stored at location {} set ZF"
                     .format(operand1,operand2[1:-1]))
        elif isOperand1AReg and isOperand2AConst:
            MakeComm(ea, "If register {} is equal to the integer constant {} set ZF"
                     .format(operand1,i))
        elif isOperand1AMem and isOperand2AConst:
            MakeComm(ea, "If the bytes stored at location {} are equal to the integer constant {} set ZF"
                     .format(operand1[1:-1],i))
        elif isOperand1AMem and isOperand2AReg:
            MakeComm(ea, "If the bytes stored at location {} are equal to the register {} set ZF"
                     .format(operand1[1:-1],operand2))

def commentRetNInstruction(opcode, ea):
    if(opcode == "retn"):
        operand = idc.GetOpnd(ea, 0)
        istr = ""
        if(operand):
            i = 0
            if(operand[-1] == 'h'):
               i = int(operand[:-1], 16)
            else:
              i = int(operand)
            istr = str(i)
        MakeComm(ea, "Transfers program control to a return address located on the top of the stack. The `n`  denotes a near return which pops only the instruction pointers (IP) register.\nThe optional source operand {} specifies the number of stack bytes to be released after the return address is popped".format(istr));


def getInstructions(block):
    ea = block.startEA
    while ea <= block.endEA:
        opcode = idc.GetMnem(ea)
        #opcodeSet.add(opcode);
        commentCallInstruction(opcode,ea)
        commentPushInstruction(opcode,ea)
        commentXORInstruction(opcode, ea)
        commentOrInstruction(opcode, ea)
        commentMovInstruction(opcode, ea)
        commentMovzxInstruction(opcode, ea)
        commentMovsxInstruction(opcode, ea)
        commentLeaInstruction(opcode, ea)
        commentSubInstruction(opcode, ea)
        commentTestInstruction(opcode, ea)
        commentAddInstruction(opcode, ea)
        commentPopInstruction(opcode, ea)
        commentCmpInstruction(opcode, ea)
        commentJZInstruction(opcode, ea)
        commentJNZInstruction(opcode, ea)
        commentJmpInstruction(opcode, ea)
        commentJlInstruction(opcode, ea)
        commentJleInstruction(opcode, ea)
        commentJgInstruction(opcode, ea)
        commentJgeInstruction(opcode, ea)
        commentJaInstruction(opcode, ea)
        commentJbInstruction(opcode, ea)
        commentJbeInstruction(opcode, ea)
        commentJnbInstruction(opcode, ea)
        commentLoopInstructions(opcode, ea) #Note: run this after all jumps
        commentLeaveInstruction(opcode, ea)
        commentRetNInstruction(opcode, ea)
        commentAndInstruction(opcode, ea)
        commentIncInstruction(opcode, ea)
        commentDecInstruction(opcode, ea)
        commentAndInstruction(opcode, ea)
        commentIMulInstruction(opcode, ea)
        commentIDivInstruction(opcode, ea)
        commentCdqInstruction(opcode, ea)
        commentSarInstruction(opcode, ea)
        commentShrInstruction(opcode, ea)
        commentShlInstruction(opcode, ea)
        commentSetnlInstruction(opcode, ea)
        commentStosdInstruction(opcode, ea)
        ea = idc.NextHead(ea)

def getBasicBlock(func):
    flow = idaapi.FlowChart(func)
    edges = [];
    for block in flow:
        getInstructions(block)
        #print "basic block id: {}".format(block.id)
        for succBlock in block.succs():
            #print "succ basic block id: {}".format(succBlock.id)
            edge = (block.id, succBlock.id)
            edges.append(edge)

    if(shouldGraph):
        with open('bbFunctionIds/{}.txt'.format(get_func_name2(func.startEA)), 'w') as f:
            print >> f, edges

